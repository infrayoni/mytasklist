(function() {
    
    var taskManager = TaskManager.getInstance();
    
    $(document).on("pageinit", "#taskCreation", function(e) {
        e.preventDefault();
        
        $("#saveTask").on("tap", function() {
            e.preventDefault();
			
			if ($("#title").val() && $("#desc").val() && $("#date").val())
			{
				var taskItem = new TaskItem($("#title").val(), 
											  $("#desc").val(),
											  $("#date").val(),
											  $("#vid").val());
				
				var parts = ($("#date").val()).split("/");
				var dateForComperasion = new Date(parts[2], parts[0] - 1, parts[1], 8, 0, 0, 0).getTime();
				
				if (dateForComperasion <= new Date())
				{
					device.showToast("Due date must be after Today!");
				}
				
				else
				{
					device.cancelReminder(taskItem.id, taskItem.title, taskItem.desc);
					device.addReminder(taskItem.id, taskItem.title, taskItem.desc, dateForComperasion.toString());
					taskManager.saveTask(taskItem);
					device.showToast("Reminder Saved: " + taskItem.title);
					$.mobile.changePage("#taskList");
				}
			}
			
			else
			{
				device.showToast("All fields must be filled!");
			}
        }); 
    });
    
    $(document).on("pageshow", "#taskCreation", function(e) {
        e.preventDefault();
        
        var taskID = ($.mobile.pageData && $.mobile.pageData.taskID) ? $.mobile.pageData.taskID : null;
        var taskItem = new TaskItem("", "", "", "");
        
        if (taskID) {
            //Update an existing task
            taskItem = taskManager.getTaskDetails(taskID);
        } 
        
        populateTaskFields(taskItem);
    });
    
    function populateTaskFields(taskItem) {
        $("#title").val(taskItem.title);
        $("#desc").val(taskItem.desc);
		$("#date").val(taskItem.date);
		$("#vid").val(taskItem.id);
    }
})();
