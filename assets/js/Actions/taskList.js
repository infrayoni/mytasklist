(function() {
	var taskManager = TaskManager.getInstance();
	
    $(document).on("pageinit", "#taskList", function(e) {
    	
    	$("#cancelAllTasks").on("tap", function() {
    		e.preventDefault();
			var tasks = taskManager.getTasks();
				
			if (!jQuery.isEmptyObject(tasks)) 
			{
				for (var task in tasks)
				{
					device.cancelReminder(tasks[task].id, tasks[task].title, tasks[task].desc);
				}
				
				taskManager.cancelAllTasks();
				device.showToast("Cancelled all reminders!");
			}
		
			updateTaskList();
    	});      
    });
    
    $(document).on("pageshow", "#taskList", function(e) {
        e.preventDefault();
        removeOutdatedTasks();
        updateTaskList();
    });
    
    function updateTaskList() {
		device.log("Started: updateTaskList");
		var tasks = taskManager.getTasks();
		
        $("#taskListView").empty();
         
        if (jQuery.isEmptyObject(tasks))
        {
        	device.log("Started: No Tasks in List");
            $("<li>No Tasks Available</li>").appendTo("#taskListView");
        } 
        
        else
        {
        	device.log("Found Tasks in List");
            for (var task in tasks) 
            {
                $("<li><a href='#taskCreation?taskID=" + tasks[task].id + "'>" + 
            		tasks[task].title + " - Due Date: " + tasks[task].date + "</a></li>").appendTo("#taskListView");
            }
        }
        
        $("#taskListView").listview('refresh');  
        device.log("Ended: updateTaskList");  	
    }
    
    function removeOutdatedTasks()
    {
    	device.log("Started: removeOutdatedTasks");
		var tasks = taskManager.getTasks();
   		
        for (var task in tasks) 
        { 
        	var parts = tasks[task].date.split("/");
			var dateForComperasion = new Date(parts[2], parts[0] - 1, parts[1], 8, 0, 0, 0).getTime();
			
		    if (dateForComperasion <= new Date())
	        {
	        	taskManager.cancelTask(tasks[task]);
	        }
        }
        
       device.log("Ended: removeOutdatedTasks");
    }
    
})();
