(function() {
    
    var taskManager = TaskManager.getInstance();
    
    $(document).on("pageinit", "#taskCreation", function(e) {
        e.preventDefault();
        
        $("#cancelTask").on("tap", function() {
            e.preventDefault();

            var taskItem = new TaskItem($("#title").val() || "Untitled", 
                                          $("#desc").val() || "",
										  $("#date").val(),
										  $("#vid").val() || null);
            
			device.cancelReminder(taskItem.id, taskItem.title, taskItem.desc);
            device.showToast("Reminder Cancelled!");
            
            taskManager.cancelTask(taskItem);            
            $.mobile.changePage("#taskList");
        }); 
    });
    
    $(document).on("pageshow", "#taskCreation", function(e) {
        e.preventDefault();
        
        var taskID = ($.mobile.pageData && $.mobile.pageData.taskID) ? $.mobile.pageData.taskID : null;
        var taskItem = new TaskItem("", "", "", "");
        
        if (taskID) {         
            //Update an existing task
            taskItem = taskManager.getTaskDetails(taskID);
        } 
        
        populateTaskFields(taskItem);
    });
    
    function populateTaskFields(taskItem) {
        $("#title").val(taskItem.title);
        $("#desc").val(taskItem.desc);
		$("#date").val(taskItem.date);
		$("#vid").val(taskItem.id);
    }
})();
