(function() {
	$(document).bind("pagebeforechange", function(event, data) {
	    $.mobile.pageData = (data && data.options && data.options.pageData) 
	    				  ? data.options.pageData : null;
	});
})();