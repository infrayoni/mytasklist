var TaskManager = (function () {     
  var instance;
 
  function createObject() {
      var cacheManager = CacheManager.getInstance();
      var TASKS_KEY = "tasks";
      var tasksMap;
      
      return {
          getTasks: function () {
              tasksMap = cacheManager.get(TASKS_KEY) || {};
              
              return tasksMap;
          }, 
          getTaskDetails: function (taskID) {
              tasksMap = cacheManager.get(TASKS_KEY) || {};

              return tasksMap[taskID];
          },
          saveTask: function (taskItem) {  
              tasksMap = cacheManager.get(TASKS_KEY) || {};
              tasksMap[taskItem.id] = taskItem;
             
			 cacheManager.put(TASKS_KEY, tasksMap);
          },
		  cancelTask: function (taskItem) {
			 tasksMap = cacheManager.get(TASKS_KEY) || {}; 
			 delete tasksMap[taskItem.id];
			 
             cacheManager.put(TASKS_KEY, tasksMap);
          },
          cancelAllTasks: function() {
              cacheManager.remove(TASKS_KEY);
          },
    };
  };
 
  return {
    getInstance: function () {
      if (!instance) {
          instance = createObject();
      }
 
      return instance;
    }
  }; 
})();