var TaskItem = function(title, desc, date, id) {
	this.title = title || "";
	this.desc = desc || "";
	this.date = date || "";
	this.id = id || Date.now();
};

TaskItem.prototype.toString = function () {
	return "Title = " + this.title + ", " +
		   "Description = " + this.desc + ", " +
		   "ID = " + this.id + "Date = " + this.date;
};