package il.ac.hit.mytasklist.receivers;

import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import il.ac.hit.mytasklist.helpers.GeneralUtils;
import il.ac.hit.mytasklist.helpers.MyTaskListDeffs;

/** The Receiver Class of the Boot Completed Broadcasts in the application
 *  
 * @author Yoni Cohen
 * @author Rafi Russo
 */
public class BootReceiver extends BroadcastReceiver{

	/** Will re-set all the alarms that should be dispatched when reboot is completed. Using the SharedPrefs of the application.
	 * @param context The Context received in the Broadcast
	 * @param intent  The Intent received in the Broadcast
	 */
	@Override
	public void onReceive(Context context, Intent intent) 
	{
		GeneralUtils.log(this.getClass().getSimpleName(), "onReceive", "Started");
		
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED))
        {
			try
			{
				AlarmManager alarmManager = (AlarmManager) context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
				GeneralUtils.log(this.getClass().getSimpleName(), "onReceive", "Boot Completed - Restarting All Alarms!");
				Map<String, ?> prefs = PreferenceManager.getDefaultSharedPreferences(context).getAll();
				
				for (String key : prefs.keySet())
				{
					if (GeneralUtils.isValidString(key))
					{
						if (key.startsWith(MyTaskListDeffs.PREFS_ALARM_PREFIX))
						{
							GeneralUtils.log(this.getClass().getSimpleName(), "onReceive", "Found Pref Key: " + key);
							String sJobj = (String) prefs.get(key);
							JSONObject jObjValues = new JSONObject(sJobj);
							
							if (jObjValues.length() == MyTaskListDeffs.SHARED_PREFS_VALUES_NUMBER)
							{
								long nTime = Long.parseLong(jObjValues.getString(MyTaskListDeffs.NOTIFICATION_TIME));
								PendingIntent toAdd = GeneralUtils.getPendingIntent(context, jObjValues.getString(MyTaskListDeffs.NOTIFICATION_ID), jObjValues.getString(MyTaskListDeffs.NOTIFICATION_TITLE), jObjValues.getString(MyTaskListDeffs.NOTIFICATION_DESC));
								GeneralUtils.setAlarm(alarmManager, toAdd, nTime);
							}
						}
					}
				}
			} catch (NumberFormatException e) {
				GeneralUtils.log(this.getClass().getSimpleName(), "onReceive", "Caught NumberFormatException");
			} catch (JSONException e) {
				GeneralUtils.log(this.getClass().getSimpleName(), "onReceive", "Caught JSONException");
			}
		}
	}
}