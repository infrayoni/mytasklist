package il.ac.hit.mytasklist.receivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import il.ac.hit.mytasklist.R;
import il.ac.hit.mytasklist.helpers.GeneralUtils;
import il.ac.hit.mytasklist.helpers.MyTaskListDeffs;

/** The Receiver Class of the Alarm Broadcasts in the application
 *  
 * @author Yoni Cohen
 * @author Rafi Russo
 */
public class AlarmReceiver extends BroadcastReceiver{

	/** Will dispatch a notification request to the Notification Manager when called according to the Input Intent
	 * @param context The Context received in the Broadcast
	 * @param intent  The Intent received in the Broadcast
	 */
	@Override
	public void onReceive(Context context, Intent intent) 
	{
		GeneralUtils.log(this.getClass().getSimpleName(), "onReceive", "Started");
		
		if (intent.getAction().equals(MyTaskListDeffs.RECEIVER_ACTION))
	    {
			GeneralUtils.log(this.getClass().getSimpleName(), "onReceive", "Received Alarm Intent - Time To Notify!");
			
		    int nId = intent.getIntExtra(MyTaskListDeffs.NOTIFICATION_ID, (int) Math.random());
			String sId = intent.getStringExtra(MyTaskListDeffs.NOTIFICATION_ID_INTENT);
		    String sTitle = intent.getStringExtra(MyTaskListDeffs.NOTIFICATION_TITLE);
			String sDescription = intent.getStringExtra(MyTaskListDeffs.NOTIFICATION_DESC);
			
			GeneralUtils.log(this.getClass().getSimpleName(), "onReceive", "Finished getting Extras from Intent");
			
	    	if (GeneralUtils.isValidString(sId) && GeneralUtils.isValidString(sTitle)
	    			&& GeneralUtils.isValidString(sDescription))
			{
		        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
					.setSmallIcon(R.drawable.ic_launcher)
					.setContentTitle(sTitle)
					.setContentText(sDescription)
		        	.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
		        	.setTicker(sDescription)
		    		.setVibrate(new long[]{500,500,500});
		    	
		        GeneralUtils.log(this.getClass().getSimpleName(), "onReceive", "Finished with NotificationCompat.Builder");
		        
		        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		        mNotificationManager.notify(nId, notificationBuilder.build());
		        
		        SharedPreferences.Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
		        prefsEditor.remove(MyTaskListDeffs.PREFS_ALARM_PREFIX + sId);
		        prefsEditor.commit();
		        
		        GeneralUtils.log(this.getClass().getSimpleName(), "onReceive", "Notified!" + " [Intent Id]: " + sId + " [Title]: " + sTitle + " [Description]: " + sDescription);
			}
	    }
		
		else
		{
			GeneralUtils.log(this.getClass().getSimpleName(), "onReceive", "Failed to send notification due to invalid arguements");
		}
	}
}