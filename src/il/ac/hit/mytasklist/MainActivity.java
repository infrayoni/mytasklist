package il.ac.hit.mytasklist;

import il.ac.hit.mytasklist.R;
import il.ac.hit.mytasklist.helpers.MyTaskListDeffs;
import il.ac.hit.mytasklist.helpers.GeneralUtils;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

@SuppressLint({ "SetJavaScriptEnabled", "NewApi" })
@SuppressWarnings("deprecation")
public class MainActivity extends Activity {

	private WebView m_webView;
	
	/* We decided to use onResume, since onCreate is not always called.
	 Initating the WebView on onResume, makes sure the HTML page always stays refreshed (ie. if Tasks need to be removed as they are outdated) */
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        GeneralUtils.log(this.getClass().getSimpleName(), "onCreate", "Started");
    }
	
	protected void onResume() {
        super.onResume();
        
        GeneralUtils.log(this.getClass().getSimpleName(), "onResume", "Started");
        initWebView();
    }
	
	private void initWebView()
	{
		if (m_webView == null)
		{
			m_webView = (WebView) findViewById(R.id.webview);
			m_webView.addJavascriptInterface(new WebAppInterface(this), "device");
			WebSettings webViewSettings = m_webView.getSettings();
			webViewSettings.setJavaScriptEnabled(true);
			webViewSettings.setDatabaseEnabled(true);
			webViewSettings.setDomStorageEnabled(true);
			webViewSettings.setAllowContentAccess(true);
			webViewSettings.setAllowFileAccessFromFileURLs(true);
			webViewSettings.setAllowFileAccess(true);
			webViewSettings.setAllowUniversalAccessFromFileURLs(true);
			webViewSettings.setAppCacheEnabled(true);
			webViewSettings.setDatabasePath(getFilesDir().getPath() + getPackageName() +"/databases/");
			webViewSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
			m_webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			m_webView.setWebChromeClient(new WebChromeClient());
		}
		
		m_webView.loadUrl(MyTaskListDeffs.FILE_ASSET_PREFIX + "index.html");
	}
}