package il.ac.hit.mytasklist.helpers;

/** The application unique exception
 * @author Yoni Cohen
 * @author Rafi Russo */
public class MyTaskListException extends Exception {

	private static final long serialVersionUID = 545656972L;
	
	/** Constructs a new MyTaskListException that includes the current stack trace */
	public MyTaskListException() {
	}

	/** Constructs a new MyTaskListException with the current stack trace and the specified detail message.
	* @param The detail message for this exception. */
	public MyTaskListException(String detailMessage) {
		super(detailMessage);
	}

	/** Constructs a new MyTaskListException with the current stack trace and the specified cause.
	* @param throwable The cause of this exception. */
	public MyTaskListException(Throwable throwable) {
		super(throwable);
	}

	/** Constructs a new MyTaskListException with the current stack trace, the specified detail message and the specified cause.
	* @param detailMessage The detail message for this exception.
	* @param throwable	The cause of this exception. */
	public MyTaskListException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}
}