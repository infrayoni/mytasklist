package il.ac.hit.mytasklist.helpers;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.util.Log;

/** GeneralUtils class will contain static service methods of different types that will be used all over the different application components.
 * 
 * @author Yoni Cohen
 * @author Rafi Russo
 */
public class GeneralUtils {
	
	private GeneralUtils() {}
	
	/** Checks if the string is valid string (Object not null, length is larger than 0 and that is not whitespaces only)
	 * @param sToCheck String to check if valid
	 * @return true if string is valid, false otherwise
	 */
	public static boolean isValidString(String sToCheck)
	{
		return sToCheck != null && sToCheck.length() > 0 && sToCheck.trim().length() > 0;
	}
	
	/** Logs to the device Logcat - Using "MyTaskList" as the Tag
	 * @param format The string/format to log
	 * @param args Optional - An object array containing objects used in the format */
	public static void log(String format, Object ... args)
	{
		String msg = "[" + System.currentTimeMillis() + "]";
		msg += String.format(format, args);
		Log.d("MyTaskList", msg);
	}
	
	/** Logs a detailed message to the device Logcat  - Using "MyTaskList" as the Tag
	 * @param cls The name of calling class
	 * @param method The name of the calling method
	 * @param format The string/format to log
	 * @param args Optional - An object array containing objects used in the format */
	public static void log(String clsName, String methodName, String format, Object ... args)
	{
		String msg = "[" + clsName + ":" + methodName + "] ";
		msg += String.format(format, args);
		log(msg);
	}
	
    /** Creates unique PendingIntents for each reminder 
     * @param context Context to be used later in getBroadcast()
     * @param sId ID for the PendingIntent and the Notification
     * @param sTitle Title of the Notification
     * @param sDescription Description of the Notification (ie. The text displayed)
     * @return The PendingIntent Object created. */
	public static PendingIntent getPendingIntent(Context context, String sId, String sTitle, String sDescription)
    {
		log("GeneralUtils", "getPendingIntent", "Started");
		PendingIntent piRet = null;
		
        try
    	{
        	if (isValidString(sId) && isValidString(sTitle) && isValidString(sDescription))
        	{
	        	long nLongId = Long.parseLong(sId);
		    	int nUniqueId = ((int) (nLongId / 1024) + (int) (nLongId % 1024));
		    	
	        	Intent intent = new Intent(MyTaskListDeffs.RECEIVER_ACTION);
	        	intent.putExtra(MyTaskListDeffs.NOTIFICATION_ID_INTENT, sId);
	        	intent.putExtra(MyTaskListDeffs.NOTIFICATION_ID, nUniqueId);
	           	intent.putExtra(MyTaskListDeffs.NOTIFICATION_TITLE, sTitle);
		    	intent.putExtra(MyTaskListDeffs.NOTIFICATION_DESC, sDescription);
		    	
		    	log("GeneralUtils", "getPendingIntent", "Created new Intent");
		    	
		    	piRet = PendingIntent.getBroadcast(context, nUniqueId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	        }
        }
        
        catch (NumberFormatException e)
        {
        	log("GeneralUtils", "getPendingIntent", "Caught NumberFormatException when trying to parseLong()");
        }
        
		return piRet;
	}
    
    /** Sets an Alaram to be dispatched in the future
     * 
     * @param alarmManager An AlarmManager Object to set the Alaram on
     * @param pi The PendingIntent for the Alarm
     * @param nTime Dispatch time for the Alarm
     */
    @SuppressLint("NewApi")
    public static void setAlarm(AlarmManager alarmManager, PendingIntent pi, long nTime)
    {
    	if (VERSION.SDK_INT >= VERSION_CODES.KITKAT)
		{
    		alarmManager.setExact(AlarmManager.RTC_WAKEUP, nTime, pi);
		}
		
		else
		{
			alarmManager.set(AlarmManager.RTC_WAKEUP, nTime, pi);	
		}
    	
    	log("GeneralUtils", "setAlarm", "Successfully set Alarm for " + String.valueOf(nTime));
    }
}