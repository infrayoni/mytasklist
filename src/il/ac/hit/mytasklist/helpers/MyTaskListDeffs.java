package il.ac.hit.mytasklist.helpers;

/** Class that holds definitions to be used over the application components.
 *  We decided to use it mainly to keep things organized.
 *  
 * @author Yoni Cohen
 * @author Rafi Russo
 */

public class MyTaskListDeffs {

	private MyTaskListDeffs() {}
	
	/** The Asset Folder Relative Path */ 
	public final static String FILE_ASSET_PREFIX = "file:///android_asset/";

	/** The Broadcast Receiver Action String to be compared later */ 
	public final static String RECEIVER_ACTION = "MTL_Alarm";

	/** The Prefix of each Alarm JSON saved in the Application Shared Prefs */ 
	public final static String PREFS_ALARM_PREFIX = "MTL_PI_";
	
	/** Notification ID for the Intent */
	public final static String NOTIFICATION_ID_INTENT = "MTL_id_str";

	/** Notification ID */
	public final static String NOTIFICATION_ID = "MTL_id_int";
	
	/** Notification Title */
	public final static String NOTIFICATION_TITLE = "MTL_title";

	/** Notification Description/Text */
	public final static String NOTIFICATION_DESC = "MTL_description";

	/** Notification Time */
	public final static String NOTIFICATION_TIME = "MTL_time";
	
	/** Number of values per a Shared Pref Item */
	public final static int SHARED_PREFS_VALUES_NUMBER = 4;
}
