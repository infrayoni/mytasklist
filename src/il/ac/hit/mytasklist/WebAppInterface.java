package il.ac.hit.mytasklist;

import org.json.JSONException;
import org.json.JSONObject;

import il.ac.hit.mytasklist.helpers.GeneralUtils;
import il.ac.hit.mytasklist.helpers.MyTaskListDeffs;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

/** The Class that lets the Android Native Side interact with the JavaScript/WebView Side of the Application
 * 
 * @author Yoni Cohen
 * @author Rafi Russo
 */
public class WebAppInterface {
	private Context mContext;
	private AlarmManager mAlarmManager;
	private SharedPreferences.Editor mPrefsEditor;
    
    /** Instantiate the interface and set the context
     * 
     * @param c Context to be used over the component
     */
    @SuppressLint("CommitPrefEdits")
	WebAppInterface(Context context) {
        mContext = context;
        mAlarmManager = (AlarmManager) mContext.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mPrefsEditor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
    }

    /** Shows toast on screen
     *  
     * @param sMessage The message to be displayed on the screen
     */
    @JavascriptInterface
    public void showToast(String sMessage)
    {
    	if (sMessage.length() != 0)
    	{
    		Toast.makeText(mContext, sMessage, Toast.LENGTH_LONG).show();
    	}
    }
    
    /** Lets the JavaScript interface log to the device logcat
     * 
     * @param sMessage The message to be logged
     */
    @JavascriptInterface
    public void log(String sMessage)
    {
    	GeneralUtils.log(this.getClass().getSimpleName(), "log", "[JavaScript]: " + sMessage);
    }
    
    /** Adds a task reminder
     * 
     * @param sId The ID of the Task Reminder
     * @param sTitle The Title of the Task Reminder
     * @param sDescription The Description of the Task Reminder
     * @param sTime The Time until the Task Reminder/Alarm should be dispatched
     */
    @JavascriptInterface
    @SuppressLint("NewApi")
	public void addReminder(final String sId, final String sTitle, final String sDescription, final String sTime)
    {
    	GeneralUtils.log(this.getClass().getSimpleName(), "updateTask", "Started");
        
    	try
    	{
	    	if (GeneralUtils.isValidString(sId) && GeneralUtils.isValidString(sTitle)
	    			&& GeneralUtils.isValidString(sDescription) && GeneralUtils.isValidString(sTime))
	    	{
    			long nTime = Long.parseLong(sTime);
	    	
    			PendingIntent toAdd = GeneralUtils.getPendingIntent(mContext, sId, sTitle, sDescription);
	    	
		    	if (toAdd != null)
		    	{
		    		GeneralUtils.log(this.getClass().getSimpleName(), "updateTask", "PendingIntent not null!");
		    		
		    		GeneralUtils.setAlarm(mAlarmManager, toAdd, nTime);
		    		
	    			JSONObject jObj = new JSONObject();
	    			jObj.put(MyTaskListDeffs.NOTIFICATION_ID, sId);
	    			jObj.put(MyTaskListDeffs.NOTIFICATION_TITLE, sTitle);
	    			jObj.put(MyTaskListDeffs.NOTIFICATION_DESC, sDescription);
	    			jObj.put(MyTaskListDeffs.NOTIFICATION_TIME,  sTime);
	    			
		    		mPrefsEditor.putString(MyTaskListDeffs.PREFS_ALARM_PREFIX + sId, jObj.toString());
		    		mPrefsEditor.commit();
		    		
		    		GeneralUtils.log(this.getClass().getSimpleName(), "updateTask", "Success");
		    	}
	    	}
    	
	    	else
	    	{
	    		GeneralUtils.log(this.getClass().getSimpleName(), "updateTask", "Failed due to invalid arguements");
	    	}
    	}
    	
		catch (NumberFormatException e)
		{
			GeneralUtils.log(this.getClass().getSimpleName(), "updateTask", "Caught NumberFormatExecption when trying to parseLong()");
		}
    	
    	catch (JSONException e)
		{
			GeneralUtils.log(this.getClass().getSimpleName(), "updateTask", "Caught JSONException when trying to insert into Prefs Object");
		}
    }
   
    /** Cancel a task reminder, not just ID is needed since we want to create the exact same PendingIntent Object (Documented in the Android API)
     * 
     * @param sId The ID of the Task Reminder
     * @param sTitle The Title of the Task Reminder
     * @param sDescription The Description of the Task Reminder
     */
    @JavascriptInterface
    public void cancelReminder(String sId, String sTitle, String sDescription)
    {
    	GeneralUtils.log(this.getClass().getSimpleName(), "updateTask", "Started");
    	
    	PendingIntent toCancel = GeneralUtils.getPendingIntent(mContext, sId, sTitle, sDescription);
    	
    	if (GeneralUtils.isValidString(sId) && GeneralUtils.isValidString(sTitle)
    			&& GeneralUtils.isValidString(sDescription))
    	{
	    	if (toCancel != null)
	    	{
	    		mAlarmManager.cancel(toCancel);
	    		mPrefsEditor.remove(MyTaskListDeffs.PREFS_ALARM_PREFIX + sId);
	    		mPrefsEditor.commit();
	    		
	    		GeneralUtils.log(this.getClass().getSimpleName(), "updateTask", "Ended: Success");
	    	}
    	}
    	
    	else
    	{
    		GeneralUtils.log(this.getClass().getSimpleName(), "updateTask", "Failed due to invalid arguements");
    	}   	
     }
}